<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Entity\Campaign;
use App\Entity\Participant;
use App\Form\SpendingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{

    /**
     * @Route("/spend", name="spending", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        //Récupération des données POSTEn registrement en bdd pour le participant
        $participant = new Participant();

        $campaign_id = $request->request->get('campaign_id');
        
        //Instancier la campagne
        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);


        // $participant->setCampaignId($request->request->get('campaign_id'));
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

       

        //Enregistrement en base de données
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $spending = new Spending();

        // on récupère les donnée dans le input avec le name amount et label
        $amount =(int)$request->request->get('amount') * 100;
        $label = $request->request->get('label');

        // on change/ajoute une valeur dans la base (les nom de colonne)
        $spending->setAmount($amount);
        $spending->setParticipant($participant);
        $spending->setLabel($label);

        //Enregistrement en base de données
        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $request->request->get('campaign_id')
        ]);
        
    }

 

   
}
