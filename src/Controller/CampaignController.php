<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Participant;
use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
    /**
     * @Route("/", name="campaign_index", methods="GET")
     */
    public function index(): Response
    {
        $campaigns = $this->getDoctrine()
            ->getRepository(Campaign::class)
            ->findAll();

        return $this->render('campaign/index.html.twig', ['campaigns' => $campaigns]);
    }

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        // récupérer les inupt du form
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        // récupérer l'input name on peut aussi le récuperer dans campainType si on ajoute le champs
        // $campaign->setAuthor($request->request->get('author'));
        
       
        if (isset($_GET['campaign_title'])) {
            $campaign_title = $_GET['campaign_title'];
        }
        

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', [
                'id' => $campaign->getId()
            ]);
        }

        if (isset($_GET['campaign_title'])) {
    
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
                'campaign_title' => $campaign_title,
            ]);
        } else {
            return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
            ]);
        }

    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET|POST")
     */

    public function show(Campaign $campaign): Response
    {
        // payemnt_ amount est relier à participant et spending_amount relier a amount spending
        $query = 'SELECT participant.*, payment.amount as payment_amount, spending.amount as spending_amount, spending.label as spending_label FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        LEFT JOIN spending ON spending.participant_id = participant.id
        WHERE campaign_id = "'.$campaign->getId() .'"' ;
        
        $statement =  $this->getDoctrine()
                            ->getManager()
                            ->getConnection()
                            ->prepare($query);

        $statement->execute();
        
        $participantWithParticipations = $statement->fetchAll();

        $totalAmount=0;
        $totalParticipant= count($participantWithParticipations);
        

        for ( $i=0 ; $i <= ($totalParticipant -1); $i++)
        {
            $totalAmount += $participantWithParticipations[$i]["payment_amount"];
            $totalAmount += $participantWithParticipations[$i]["spending_amount"];
        }
        $totalAmount_cent = $totalAmount / 100;
        //valeur en Pourcentage
        $objectif = round(($totalAmount_cent * 100)/ $campaign->getGoal());

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations','totalAmount', 'totalParticipant', 'objectif'));
        
    }

      /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_show', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }

     /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Request $request, Campaign $campaign): Response
    {
        //$amount_participent = $_GET['amount_participent'];
        //$amount_participant = $request->query->get('amount_participant');
        $amount_participant = $_GET['amount_participant'] ?? "";
  
        //dd($amount_participent);
        return $this->render('campaign/pay.html.twig', compact('campaign', 'amount_participant'));
    }

    /**
     * @Route("/{id}/spending", name="campaign_spending", methods="GET|POST")
     */
    public function spend (Request $request, Campaign $campaign): Response
    {
        return $this->render('spending/showSpending.html.twig', compact('campaign'));
    }
}
