<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Entity\Payment;
use App\Entity\Campaign;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    
    /**
     * @Route("/pay", name="payment_charge", methods="GET|POST")
     */
    public function pay(Request $request): Response
    {
        
        try {

            //Avoir la participation en centimes
            $amount = (int)$request->request->get('amount') * 100;
            \Stripe\Stripe::setApiKey('sk_test_SmEOatgIrXR09ZuYK5UZAGpm');
            $charge = \Stripe\Charge::create([
                'amount' => $amount, 
                'currency' => 'eur', 
                'source' => $request->request->get('stripeToken')
                ]);

        } catch(\Exception $e) {
            
            $this->addFlash(
                'error',
                'Le paiement à échoué. Raison : ' . $e->getMessage()
            );

            return $this->redirectToRoute('campaign_pay', [
                'id' => $campaign_id
            ]);

            //TODO 
        }

        $campaign_id = $request->request->get('campaign_id');

        //Instancier la campagne
        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);

        //Récupération des données POSTEnregistrement en bdd pour le participant
        $participant = new Participant();
        // $participant->setCampaignId($request->request->get('campaign_id'));
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));


        //Enregistrement en base de données
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $payment = new Payment();
        $payment->setAmount($amount);
        $payment->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $campaign_id
        ]);
    }

   
    
}
